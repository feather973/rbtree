#include <stddef.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>

#define LEFT	0
#define RIGHT	1

#define RED		0
#define BLACK	1

#define left	child[LEFT]
#define right	child[RIGHT]

struct node {
	int val;
	struct node *parent;
	struct node *child[2];
	int color;
};

struct tree { 
	struct node *root;
};

int insert(int val);
struct node *search(int val);
void trav(void);
int erase(int val);

struct tree g_root;

#define childDir(n)		((n->parent->right == n) ? RIGHT : LEFT)

// p(subtree) rotate dir
// ex) p rotate right
// 
//     g              g
//   p       -->    s
// s              d   p
//d c                c
//
// check parent(p), grandparent(g), sibling(s), inner child(c)
static struct node *rotate(struct node *p, int dir)
{
	struct node *c, *g, *s;

	g = p->parent;

	s = p->child[1-dir];
	if (!s) {
		printf("[WARN] s null\n");
		return NULL;
	}

	c = s->child[dir];
	
	p->child[1-dir] = c;
	if (c)
		c->parent = p;
	s->child[dir] = p;
	p->parent = s;

	s->parent = g;
	if (g)
		g->child[ p == g->right ? RIGHT : LEFT ] = s;
	else
		g_root.root = s;

	return s;		// new subtree root
}

// check parent(p), grandparent(g), uncle(u)
static void insert_rebal(struct node *leaf)
{
	struct node *n, *p, *g, *u;
	int p_dir;

	n = leaf;

	while (n->parent) {
		p = n->parent;
		if (p->color == BLACK) {
			return;		/* case 1 */
		}

		// from now on, p is RED ( == g is BLACK )
		if (!(p->parent)) {
			p->color = BLACK;
			return;		/* case 4 */
		}

		// rbb
		g = p->parent;
		if (g->val < p->val)
			p_dir = RIGHT;
		else
			p_dir = LEFT;

		u = g->child[1-p_dir];
		if (!u || u->color == BLACK) {
			// inner child
			if (n != p->child[p_dir]) {
				rotate(p, p_dir);
				p = g->child[p_dir];
				n = p;		// new n
			}			/* case 5 */

			// outer child	
			rotate(g, 1-p_dir);
			p->color = BLACK;
			g->color = RED;
			return;		/* case 6 */
		}
		
		// rbr
		p->color = BLACK;
		g->color = RED;
		if (u)
			u->color = BLACK;	
		n = g;		// new n
		p = n->parent;
		continue;	/* case 2 */
	}

	return;		/* case 3 */
}

int insert(int val)
{
	struct node *cur, *parent, *leaf;
	int ret = 0;

	printf("insert %d\n", val);

	// emtry tree
	if (!g_root.root) {
		g_root.root = calloc(1, sizeof(struct node));
		if (!g_root.root)
			return -ENOMEM;

		g_root.root->val = val;
		g_root.root->parent = NULL;
		g_root.root->color = RED;
		return ret;
	}

	cur = parent = g_root.root;

	// find leaf
	while (cur) {
		if (cur->val == val) {
			ret = -1;		/* dup */
			break;
		} else if (cur->val < val) {
			parent = cur;
			cur = cur->child[RIGHT];
		} else {
			parent = cur;
			cur = cur->child[LEFT];
		}
	}

	if (ret < 0)
		return ret;

	// alloc node
	leaf = calloc(1, sizeof(struct node));
	if (!leaf)
		return -ENOMEM;

	leaf->val = val;
	if (parent->val < leaf->val)
		parent->child[RIGHT] = leaf;
	else
		parent->child[LEFT] = leaf;
	leaf->parent = parent;
	leaf->color = RED;

	// rebalance
	insert_rebal(leaf);
}

struct node *search(int val)
{
	/* TBD */
	return NULL;
}

void __trav(struct node *n)
{
	if (!n)
		return;

	__trav(n->child[LEFT]);
	printf("%d ", n->val);
	__trav(n->child[RIGHT]);	
}

void trav(void)
{
	__trav(g_root.root);	
	printf("\n");
	return;
}

// check parent(p), sibling(s), inner child(c), outer child(d)
static void black_leaf_erase(struct node *n)
{
	struct node *p, *s, *c, *d;
	int dir;

	// free node
	p = n->parent;
	dir = childDir(n);

	p->child[dir] = NULL;
	free(n);
	goto startD;

	// rebalance
	do {
		dir = childDir(n);
startD:
		s = p->child[1-dir];
		
		if (!s)
			return;

		c = s->child[dir];
		d = s->child[1-dir];

		if (s->color == RED)
			goto D3;

		if (d && d->color == RED)
			goto D6;
	
		if (c && c->color == RED)
			goto D5;

		// c and d is NULL or black
		if (p->color == RED)
			goto D4;

D3:
		rotate(p, dir);
		p->color = RED;
		s->color = BLACK;

		s = c;
		if (!s)
			return;

		d = s->child[1-dir];
		if (d && d->color == RED)
			goto D6;
		c = s->child[dir];
		if (c && c->color == RED)
			goto D5;

	// D2
		s->color = RED;
		n = p;
	} while (p = n->parent);

	// D1: p is NULL
	return;

D4:
	s->color = RED;
	p->color = BLACK;
	return;

D5:
	rotate(s, 1-dir);
	s->color = RED;
	c->color = BLACK;
	d = s;
	s = c;

D6:
	rotate(p, dir);
	s->color = p->color;
	p->color = BLACK;
	s->color = BLACK;
	return;
}

int simple_erase(struct node *cur)
{
	int dir;
	struct node *p, *c;
	struct node *c1, *c2;		// c's child

	// cur is black, one child ( cur cannot red )
	if (cur->left && !cur->right) {
		c = cur->left;
	} else if (!cur->left && cur->right) {
		c = cur->right;
	} else {
		goto no_onechild;
	}

	cur->left = c->left;
	cur->right = c->right;

	if (cur->left)
		cur->left->parent = cur;
	if (cur->right)
		cur->right->parent = cur;

	//cur->color = c->color;
	cur->val = c->val;

	free(c);
	return 0;

no_onechild:
	// cur is red, no child
	if (cur->color == RED && !cur->left && !cur->right) { 
		if (cur->parent) {
			dir = childDir(cur);
			cur->parent->child[dir] = NULL;
		}

		free(cur);
		return 0;
	}

	//   cur          c
	//   ....         ....
	//     p   ---->    p
	//   c            c2
	//     c2
	// cur is red/black, two child
	if (cur->left && cur->right) {
		c1 = c = cur->right;

		// right's left most is p
		while (c1) {
			c1 = c->left;
			if (c1)
				c = c1;
		}

		// c2 is RED, so color can change
		c2 = c->right;
		if (c2) {
			c2->color = c->color;
			c2->parent = c->parent;
		}
		dir = childDir(c);
		c->parent->child[dir] = c2;

		// cur->color keeps 
		cur->val = c->val;

		free(c);
		return 0;
	}

	// cur is black, no child ( rebalidation is needed ) 
	return -1;	
}

int erase(int val)
{
	struct node *cur, *parent;
	int ret;
	
	printf("erase %d\n", val);

	// only root
	if (g_root.root->val == val 
			&& !g_root.root->left && !g_root.root->right) {
		free(g_root.root);
		g_root.root = NULL;
		return 0;
	}

	cur = parent = g_root.root;

	// find leaf
	while (cur) {
		if (cur->val == val) {
			break;
		} else if (cur->val < val) {
			parent = cur;
			cur = cur->right;
		} else {
			parent = cur;
			cur = cur->left;
		}
	}
	
	// free node
	if (!cur) {
		return -1;
	} 

	ret = simple_erase(cur);
	if (ret < 0) {
		black_leaf_erase(cur);		// rebalance
	}

	return 0;
}

/* STUB MAIN */
int main(int argc, char *argv[])
{
	int i, val;

	trav();

	for (i=0;i<1000;i++) {
		val = 10*i;
		insert(val);
		trav();

		val = 100*i;
		insert(val);
		trav();

		val = 50*i;
		insert(val);
		trav();
	}

	printf("delete start\n");
	for (i=0;i<1000;i++) {
		val = 50*i;
		erase(val);
		trav();

		val = 10*i;
		erase(val);
		trav();

		val = 100*i;
		erase(val);
		trav();
	}

	return 0;
}
